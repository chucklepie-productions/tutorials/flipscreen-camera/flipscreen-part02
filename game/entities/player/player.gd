extends KinematicBody2D

#movement is very simple, we move and fall at the same rate
#so no need for gravity
const SPEED:=125

var velocity:=Vector2.ZERO		#this controls the player movement and speed
var direction:=false				#default direction (facing right)

onready var sprite=$Sprite

func _ready() -> void:
	pass # Replace with function body.

# warning-ignore:unused_argument
func _physics_process(delta: float) -> void:
	_get_player_input()
	_update_player_sprite()
	_apply_gravity()
	_move_player()
	
func _get_player_input() -> void:
	#get player input and resulting velocity
	#which is really just a direction in this function
	
	#reset velocity every frame as not applying gravity
	velocity=Vector2.ZERO
	
	#get_action_strength returns a value 0-1, we subtract one from the other
	#so we get positive or negative for a direction or zero for nothing pressed
	velocity.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	#player falls naturally so only require an up vector
	velocity.y = -Input.get_action_strength("ui_up")
	
	#if we press left and up we get diagonal movement which is a vector line biger than horizontal/vertical
	#so normalise will fix this. Imagine drawing a radius on a circle, normalise
	#gives us a line to the radius. If not, then diagonal would go past the circle
	velocity=velocity.normalized()
	
	if velocity.x!=0:
		#only change direction if we are moving left/right
		#otherwise the sprite will flip at idle
		direction=false if velocity.x>0.0 else true	#are we cheating with the no else here?
	
func _update_player_sprite():
	sprite.flip_h=direction	#face right

	
func _apply_gravity():
	#we only need gravity if not moving up
	#i.e. constant speed regardless of direction
	if velocity.y>=0:
		velocity.y=1

func _move_player():
	#this must be called from _physics_process because move_and_slide works in physics loop
	#it also applies delta automatically so we do not multiple velocity by delta
	#we would if we were using move_and_collide or simply altering position directly
	#move and slide returns the remaining velocity, e.g. after a collision
	#but our simple game does not require this so return is ignored
# warning-ignore:return_value_discarded
	move_and_slide(velocity*SPEED,Vector2.UP)
