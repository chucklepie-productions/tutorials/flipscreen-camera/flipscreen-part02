extends Camera2D


func _ready() -> void:
	pass # Replace with function body.

func set_position(location:Vector2,zoom:float=1.0) -> void:
	#very simple method to set position
	#it does not check map
	#as that is responsibility of the map
	position=location
	if zoom!=1.0:
		self.zoom=Vector2(zoom,zoom)
	
