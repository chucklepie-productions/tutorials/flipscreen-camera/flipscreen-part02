# Flipscreen 2D platform games

## Part 2: Enhancing Camera and tilemap

### Overview
This is part 2. Refer to part 1 if you are here directly and have not followed part 1.

https://gitlab.com/chucklepie-productions/tutorials/flipscreen-camera/

If you only need the full solution/template or are here from the 'Create Metroid in 15 minutes' and do not wish to know how it works visit the Flipscren Template project:
https://gitlab.com/chucklepie-productions/tutorials/

Tutorial youtube videos at, rather long so maybe watch the one below instead first:
https://www.youtube.com/playlist?list=PLtc9v8

'Create Metroid in 15 minutes' using this template found here: https://www.youtube.com/watch?v=Cnx3ts86qDg


In part 1 we created the basic flipscreen camera. In this tutorial we will do 3 things:
1. Add basic starter player
2. Add layers to the tilemap to show how they work

If you are familiar with all of this (I was originally going to skip all of this) then download the code, try it out then wait for part 3.

We were going to do:
3. Main content: Expand the camera to provide flipscreen based on the player location and to also introduce navigation blocks to areas player cannot visit.

I thought the content might get too much and the scope too wide for beginners, so updating the camera based on the player will be done in the next tutorial, so there will be 4 tutorials in total. Luckily this means no more 40 minute videos to watch, only 35 ;-)

The video tutorial for this article can be found here:
https://www.youtube.com/watch?v=imZrZ8wFuc8

### Getting Started
Download the project part 2 by simply selecting the download icon above and downloading the zip, then uncompress and open within Godot. If you're feeling adventurous then use the Clone option and download everything via git.

![image.png](README_Images/image.png)

This project was saved with Godot 3.3. but should work with any 3.x version (3.2.3 if you haven't upgraded yet).

Open the Godot project, run (F5) and use the cursors to navigate the current room, the screen will not flip with the player. Ingore that the background is the wrong colour.

As before, when trying to figure things out, the important thing to remember is when looking at the node inspector, project settings, etc is simply just look for the 'revert' symbol and you will see what was changed from the default, i.e. everything else is the default, e.g.

![image-2.png](README_Images/image-2.png)


### Housekeeping
Here are basic changes made to the existing code and scenes prior to starting part 2 content:

1. Under a new top level folder (to keep it out of Godot), called support_files, I have placed the file cybernoid_128.tap. You can open this with any ZX Spectrum emulator and play the original game.
2. In Project/Settings/InputMap I added four new inputs called debug_screen_XXX and made them WASD, then in the game.gd file modified the _input to use these. This is so we can control the player with cursors but still allow screen flipping by pressing WASD. 
3. Should have been done at the start of part 1, but the tilesheet was saved as a png from a bmp. This is because bmp cannot support transparency. The tilemap was edited and all black changed to transparency. This is for a task in this part 2 to allow foreground and background layers in the tilemap. In order to change the tilemap to use the new png, the Godot project was closed, the scene file cybernoid.tscn edited and the filename changed. This will then automatically be updated on next load of the project.

Check the files/settings yourself before proceeding.

### Player
The focus of this tutorial is now the player and tile layers. Note, this is the most basic controller you will see and is not a complete player controller. We will not be using animation trees, blend trees or animation state machines for controlling animation and direction, just the simplest to get the player working. 

We will also not be adding hit/hurt boxes for enemy interaction. We are simply making the minimal viable product.

1. We create a KinematicBody2D and name it Player
2. We add a collision polygon rectangle and make its extents 16x16 (i.e. it will be 32x32 in size to cover the sprite)
3. We add a Sprite and drag the single sprite from assets/sprites/player into the texture property

#### Notes on animation
In the real world, you need animation for your sprites.

You need to choose wisely on animations, you can use a sprite sheet containing all the frames then use animation player node to animate it; you can use the sprite sheet containing all the frames then use an animated sprite to create your animations; you can even use multiple sprites and an animation player. 

The choice depends on how you are going to use it. For example if you are using an animation state machine/blend tree then you need to use an animation player, but for basic animations choose Animated Sprite node.

#### Player creation and collision setup
The best way to handle collisions is as follows.

The collision shape attached to the player generates collider results and must be checked each frame in the physics_process() method, e.g. to kill the player. However it is advisable in almost every case to never use the collision shape attached to your body for interacting with enemies or the environment like this. Instead, you should simply use the collision shape as a means of making it 'solid', i.e. use it to bounce/stop against the environment and other objects. For actual collision detection for means of interactions like death, etc, do the following:

1. Create an Area2D and call it Hurtbox. This area is larger than the collision shape and is used to receive damage from other nodes
2. Create an Area2D and call it Hitbox. This area is as the Hurtbox but is used to give damage
3. Please watch this video for guidance: https://www.youtube.com/watch?v=vDbEfmPcv-Q

In other words, in this part 2 we will only be dealing with colliding with the environment to stop the player going through the tiles.

To achieve this we do the following:
1. In project settings/layer_names/2d_physics name the first two layers: player, environment.

![image-pt2-1.png](README_Images/image-pt2-1.png)

Back in the player node find the collision section and set Layer to Player and Mask to Environment

![image-pt2-2.png](README_Images/image-pt2-2.png)

Simply: Layer is what this body is on, i.e. Player is on 'player' layer. Mask is what the body interacts with (what nodes with collisions having this id the player will hit), i.e. in our case player collision will hit anything with a 'environment' layer. 

We create a new folder called 'entities' where we will place all our 'entities' (player, enemy, items, etc) and a folder within called player and save the player node as player.

We then add an empty script and save that as player.gd in the same folder - I choose to store code with nodes per domain/functional area.


### Tilemap collisions
Open up the cybernoid.tscn file and select 'TileMap_FG1'. Ignore the 'TileMap_FG2' for now.

NOTE: An import thing to remember is to always edit the node directly if you are changing base data and not the instance. What we mean by that is if you open up map.tscn you can see our map is an instance scene (the clapper board icon) and we can modify properties there for the map. However those properties are for this instance not the actual base map. So you need to know whether you are modifying 'every' version (i.e. edit the base) or just this instance. In this case we are modifying the base.

For tilemaps you add a collision shape to every tile in the TileSet editor. However, the collision layer and mask is set at the tilemap level and so every single tile gets this same collision. To achieve different layers/masks for collision you need to create multiple tilemap layers, which will be described shortly.

Modify/check the tilemap so that it is on Layer 'environment' and looks towards Mask 'Player'. This is the opposite to how we set up player.

Edit the TileSet (click on tileset), select the graphic then click the 'Edit' button. In the last tutorial the tileset was pre-created with each tile used in the game having a 'region', i.e. it is a tile. In this tuturial each tile that blocks the player has a collision region set. Select a tile, ensure 'Collision' is clicked and each tile set as colliding with the player will show a rectangle. They are added by ensuring snap/keep within is pressed and a new rectangle added by simply double clicking inside a tile.

![image-pt2-3.png](README_Images/image-pt2-3.png)

In other words, Player will collide with the tilemap and the tilemap will collide with the player, blocking its path. Just what we want.

### Player controller
In Cybernoid, the player moves at a constant speed regardless of direction, i.e. gravity is not applied as such. The player can only move left, right, up.

The code is self-explanatory and it simply gets a direction from the player and applies a set speed to direction, applying a downward vector if up is not pressed, i.e. the player will always be falling.

Movement is handled by move_and_slide() which applies our velocity for the kinematic body and makes it slide or collide with any bodies, in our case the tilemap. It also returns a resulting velocity that we do not use here. In most games you will use this as the starting velocity for the next frame, i.e. bouncing/colliding affects the body.

Because we are not polluting our node with dependencies on other nodes, we can run this scene now (press F6) and control the player on the screen with the cursors, if you're fast enough.

#### Player facing direction
There are many ways to handle direction and rotation of sprites, but in our case all we want is to update the sprite image.

The player naturally faces right as that is the sprite we have. We define a variable 'direction' that is false for right facing and true for left. We then simply set the Sprite nodes 'flip h' parameter if facing left.

	if velocity.x!=0:
		#only change direction if we are moving left/right
		#otherwise the sprite will flip at idle
		direction=false if velocity.x>0.0 else true	#are we cheating with the no 'else' here?


We do it like this so that on idle the image will not flip back.

Note: We do not set the Player or Sprite scale.x value because doing so is a very bad idea as transforming collision shapes via scale property, which is what will happen, will cause problems.

### Player placed on map
In game.tscn we add an instance to the Player.

![image-pt2-2.png](README_Images/image-pt2-4.png)

We then drag the player and place it on it's starting spot. You can see this by selecting the Player node in the scene tree, moving mouse to the main editor and pressing 'F' to centre the editor on the node.

Run the game (F5) and you will see that the player collides with most things and will go through a few others. The screen won't move with the player, but for now use the WASD keys to keep up with the player and you can navigate the map We've almost got a game ;-)

Move left one screen, then down one screen, then try and go through the gap. If using the download then all is well, but if following by code, the player will not go through. In theory the blocks are 32x32 and the player is 32x32 so should fit. I have no idea why not. So an easy fix is go back to the player, edit the collision shape and change the extents from 16x16 to 15x15.

### Z ordering
In Godot every applicable node has a Z-order index, i.e. Z-index determines the drawing order (what will appear above or below other nodes). The default index is 0 and as such order is dictated by drawing order in the scene which is top to bottom in the scene tree. In the game.tscn, player is last so is drawn last, i.e. will be above the tilemap.

One caveat, is if you notice the property 'Z as relative'. This means (and is ticked by default), it's Z-index is added to its parent Z-index, e.g. if player had 1 but was inside a node with a z-index of 2 then its z-index would be 3 if ticked, and 1 if not.

#### Tilemap ordering
We want some tiles above the player and some below. To achieve this we need a separate tilemap. We can then control order by setting z-index (because we have all layers of the map inside 'Map' it's natural z-index is always beneath the player, or above if ordering is changed)

Open cybernoid.tscn and notice we have two tilemaps now. Using the visibility icon to the node's right, toggle each off and see what we've done. This was achieved by simply duplicating the tilemap (CTRL-D) then modifying the map (duplicate the map, in FG2 delete everything, then in FG1 cut the small number of items we need and paste into FG2, the cursor remains the same so it is quick).

Because we duplicated the node, they share resources so the tilesets will copy across but will be the same resource.

Refer to housekeeping regarding the deletion of most black pixels. The background is no longer black (it should be grey), this will be fixed in the next tutorial.

We want the FG1 layer to be below the player (we should really call it BG1), we want FG2 to be above the player.

So, keeping the Player and FG1 on the default of z-index of 0, select TileMap_FG2 and set z-index to be 1

![image-pt2-5.png](README_Images/image-pt2-5.png)

If we ever moved the Player node in the tree above the map, then the player will disappear behind FG1 because it is the same z-index, but higher up. If we think this might be the case then we should adjust, e.g. set FG1 to be -1, or set FG1, Player, FG2 to be 0,1,2 respectively.

Obviously this isn't perfect as we need some black in the tiles to avoid them being see-through, but it's fine for this demo.

Another benefit of multiple tilemaps is we can give different collision masks and layers to the tilemap. This, btw, is the way Godot handles 'layers' for tilemaps.

You can now cut/paste or update each tilemap. Observe the solution which simply moves some items into a new layer to achieve a foreground set of tiles.

### Coding practices in this tutorial
1. You may notice all the functions written begin with '_'. This signifies that the function is local to the file and can't be called from outside. This is not really true as you can call it, so it is more of a 'convention' to let you and other developers know the method won't be called. Try not to abuse this :)

2. There are lots of very small methods, the overhead is small for the Godot interpreter but it ensures we can see the entire functionality of that function on one page and ensure we do not abuse the 'single responsibility' principle too much.

3. Using 'get_parent' is a bad idea in Godot because you cannot test a node in isolation, e.g. if our player called get_parent() we could not test it by pressing F6. Another problem is if we rearrange nodes in the scene or instance them elsewhere multiple times, the parentage will change. Another reason is reducing 'tight coupling'. A good principle to follow in Godot is you can call down, e.g. $somechild.method(), but to call up (i.e. get somewhere in the parent line) it is best to create custom signals, which we will be covering in part 3.

### Bugs/Features
If you navigate your ship/screen far enough you'll notice you can't fit through a single tile gap. This is a known bug, so if you go to the player's collision shape and change it to 15x15 as a workaround.

If you use the debug menu and turn on collision shapes so you can see what is happening, there will be none for tilemaps in Godot 3.3. You now need to separately set each tilemap's 'show collision' property.

### Finish
That is all for this tutorial, otherwise it will make the next one on camera flipping with the player too long.

So, for now, play the game and move with the cursors then at the edge of a screen use WASD.

We will be updating the camera based on the player in the next, part 3.

